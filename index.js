/* s30 index.js */

// npm init-y - skips all the prompt & goes directly into installing npm/creating 
	//package.json

	/*
		create an express variable that accepts a require("express") value
		store the "express()" function inside the "app" variable
		create a port variable that accepts 3000
		make your app able to read json as well as accept data from forms
	*/


const express = require("express");

// mongoose - a package that allows creation of schemas to model the data structure and to have an 
	//access to a number of methods for manipulating the concrete database in our MongoDB
const mongoose = require("mongoose");

const app = express()

const port = 3000;




// <username> - change into the correct username in your MongoDB acct that has ADMIN function
// <password> - change into the password of the ADMIN 
 // "myFirstDatabase" - the name of the database that will be created. (changed into "b170-to-do")

mongoose.connect("mongodb+srv://mhsalanga:Koykoy22042022@wdc028-course-booking.fogfn.mongodb.net/b170-to-do?retryWrites=true&w=majority",
		{
		useNewUrlParser:true,
		useUnifiedTopology:true	
		})


// notification for connection: success/failure
let db = mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error",console.error.bind(console,"Connection Error"))
// if the connection is successful
db.once("open", () => console.log("We're connected to the database"))


app.use(express.json()); 
app.use(express.urlencoded({ extended:true})); 


// Mongoose Schema - sets the structure of the doc that is to be created; serves as the
	// blueprint to the data/record
const taskSchema = new mongoose.Schema({   //WHAT do you want to see
	name:String,
	status:{
		type:String,
		// default - sets the value once the field does not have any value entered in it
		default:"pending"  // dev-defined since it has string value
	}

})

// model - allows access to methods that will perform CRUD ops in the database;
	//RULE: the 1st letter is always capitalized/uppercase for the variable of the model
	//& must be in singular form

/*
	SYNTAX:
	const<Variable> = mongoose.model("<Variable>",<schemaName>)
*/


const Task = mongoose.model("Task", taskSchema)  // this data model is now the db


// routes
/*
	business logic:
	1. check if the task is already existing
		if the task exists, return "there is a duplicate task"
		if it is not existing, add the task in the database

	2. the task will come from the request body

	3. create a new task object with the needed properties

	4. save to db
*/ //WHEN do we want to send array, obj to client

//composed of 2 parts: task & controller
app.post("/tasks",(req,res) => {   //TASK
	// checking for duplicate tasks
	// findOne is a mongoose method that acts similar to find in MongoDB; returns the first document 
		//it finds
	//find in name field 
	Task.findOne({name:req.body.name},(error, result) => { 
		console.log(req.body.name)   //CONTROLLER   --- HOW to handle data
		// if the "result" has found a task, it will return the res.send
		if(result !== null && result.name === req.body.name){
			return res.send("There is a duplicate task");
		} else {
			// if no task is found, create the obj & save it to the db
			let newTask = new Task ({
				name:req.body.name
			})
			// saveErr - parameter that accepts errors, should there be any when saving the "newTask" 
				//object
			// savedTask- parameter that accepts the obj shd the saving of the "newTask"
				//object is a success
			newTask.save((saveErr,savedTask) => {
				// if there are errors, log in the console the error
				if (saveErr){
					return console.error(saveErr);
				} else {
					// .status - returns a status (number code such as 201 for succesful creation)
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})


/* =============================================================
	Terminal ——>
marizzehaideesalanga@Marizzes-MacBook-Pro discussion % npm start

> discussion@1.0.0 start
> node index.js

Server is running at port 3000
We're connected to the database

Postman ——>
B170 -s30
Todo-Post Request
POST
http://localhost:3000/tasks

Body - raw - json
{
    "name": "sweeping the floor"
}

Output:
New Task Created
=============================================================  */

// business logic for retrieving data *************************************
/*
1. retrieve all the documents using the find() functionality
2. if an error is encountered, print the error (error handling)
3. if there are no errors, send a success status back to the client and return an array of documents

*/

app.get( "/tasks",(req,res) => { 
	// find is similar to the mongodb/robo3t find.setting up empty field in "{}" would allow
		//the app to find all documents inside the database
	Task.find({},(error, result) => {     // {} ---> will go through entire db
		if (error){
			return console.log(error)
		} else {
			return res.status(200).json({data:result}) //data field will be set & its 
				//value  will be the result/response that we had for the rqst
		}
	})
})


// POSTMAN
// B170 -s30
// Todo post rqst  ----> POST
// http://localhost:3000/tasks
// body - raw - json
// {
//        "name": "vacuuming the floor"
//  }
// output:
// SEND
// New Task Created
// SEND
// There is a duplicate task

// B170 -s30
// get todo tasks -----> GET
// http://localhost:3000/tasks
//{
//     "data": [
//         {
//             "_id": "6262ab72164f6606b0237ab7",
//             "name": "sweeping the floor",
//             "status": "pending",
//             "__v": 0
//         },
//         {
//             "_id": "6266779d7a6b35e05db7934a",
//             "name": "washing the dishes",
//             "status": "pending",
//             "__v": 0
//         },
//         {
//             "_id": "626677de7a6b35e05db79350",
//             "name": "vacuuming the floor",
//             "status": "pending",
//             "__v": 0
//         }
//     ]
// }
//
//in mongodb, Database ----> WDC028-Course-Booking ----> Browse Collections
// b170-to-do
//   tasks

// ***************************************************************************
// create a function/route for a user registration
/*
1. Find if there is a duplicate user using the username field
	- if the user is existing, return "username already in use"
	- if the user is not existing, add it in the database
		- if the username and the password are filled, save
			- in saving, create a new User object (a User schema)
				- if there is an errror, log it in the console
				- if there is no error, send a status of 201 and "successfully registered"
		- if one of them is blank, send the response "BOTH username and password must be provided"
*/
// ********************************** s30 **********************************
const userSchema = new mongoose.Schema({
	userName: String,
	passWord: String
})

const User = mongoose.model("User", userSchema)



app.post("/signup",(req,res) => {   
	 
	User.findOne({userName:req.body.userName},(err, result) => {	
		console.log(result)
		
		if(result !== null && result.userName == req.body.userName){
			return res.send("username already in use");
		} else {
			if(req.body.userName !== "" && req.body.passWord !== ""){

			let newUser = new User ({
				userName:req.body.userName,
				passWord:req.body.passWord
			});
			
			 			
			newUser.save((saveErr,savedTask) => {
				if (saveErr){
					return console.error(saveErr);
					
				} else {
					return res.status(201).send("successfully registered");
				}
			})
		} else {
			return res.send("BOTH username and password must be provided");	
		}
	}
})

})



app.get( "/users",(req,res) => { 
	
	User.find({},(err, result) => {     // {} ---> will go through entire db
		if (err){
			return console.log(err)
		} 

		else {
			return res.status(200).json({data:result}) 
		}
	})
})


// POST SIGNUP
// http://localhost:3000/signup
// body -raw -json
// {
//     "userName":"Zoe",
//     "passWord":"123Zoe"
// }
// output: 
// successfully registered
// {
//     "userName":"Christian",
//     "passWord":""
// }
// output: 
// BOTH username and password must be provided

// GET USER
// http://localhost:3000/users
// ********************************** s30 **********************************



app.listen(port, () => console.log(`Server is running at port ${port}`)); 

















